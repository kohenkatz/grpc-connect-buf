# TODO

- Should we use [`FieldMask`](https://netflixtechblog.com/practical-api-design-at-netflix-part-1-using-protobuf-fieldmask-35cfdc606518)?
  - [Spec](https://google.aip.dev/161)

- Should we use any other specs, like [Standard Methods](https://google.aip.dev/134)?

- Is it useful to generate sample code with [protoc-gen-go-goo](https://github.com/lcmaguire/protoc-gen-go-goo)?

- Protobuf "editions" <https://protobuf.dev/news/2023-06-29/>
