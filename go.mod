module gitlab.com/kohenkatz/grpc-connect-buf

go 1.23

require (
	connectrpc.com/connect v1.18.1
	google.golang.org/protobuf v1.36.3
)
