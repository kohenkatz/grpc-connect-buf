// swift-tools-version:5.7

import PackageDescription

let package = Package(
    name: "grpc-connect-buf",
    platforms: [
        .iOS(.v12)
    ],
    products: [
        .library(
            name: "Example",
            targets: ["Example"]),
        .library(
            name: "ExampleConnect",
            targets: ["ExampleConnect"]),
        .library(
            name: "ExampleConnectMocks",
            targets: ["ExampleConnectMocks"]),
    ],
    dependencies: [
        .package(url: "https://github.com/apple/swift-protobuf.git", from: "1.28.2"),
        .package(url: "https://github.com/connectrpc/connect-swift.git", from: "1.0.0"),
    ],
    targets: [
        .target(
            name: "Example",
            dependencies: [
                .product(name: "SwiftProtobuf", package: "swift-protobuf"),
            ]
        ),
        .target(
            name: "ExampleConnect",
            dependencies: [
                .target(name: "Example"),
                .product(name: "Connect", package: "connect-swift"),
                .product(name: "ConnectNIO", package: "connect-swift"),
                .product(name: "SwiftProtobuf", package: "swift-protobuf"),
            ]
        ),
        .target(
            name: "ExampleConnectMocks",
            dependencies: [
                .target(name: "Example"),
                .target(name: "ExampleConnect"),
                .product(name: "ConnectNIO", package: "connect-swift"),
                .product(name: "ConnectMocks", package: "connect-swift"),
                .product(name: "SwiftProtobuf", package: "swift-protobuf"),
            ]
        ),
    ]
)
