#!/bin/sh
# shellcheck disable=SC3037

set +e

CI_DEPENDENCY_PROXY_SERVER_NO_PORT=$(echo -n "$CI_DEPENDENCY_PROXY_SERVER" | awk -F[:] '{print $1}')

REGISTRY_AUTH_TOKEN=$(echo -n "$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD" | base64)
CI_DEPENDENCY_PROXY_AUTH_TOKEN=$(echo -n "$CI_DEPENDENCY_PROXY_USER:$CI_DEPENDENCY_PROXY_PASSWORD" | base64)

cat > /kaniko/.docker/config.json <<EOF
{
	"auths": {
        "$CI_REGISTRY":                        { "auth": "$REGISTRY_AUTH_TOKEN" },
        "$CI_DEPENDENCY_PROXY_SERVER":         { "auth": "$CI_DEPENDENCY_PROXY_AUTH_TOKEN" },
        "$CI_DEPENDENCY_PROXY_SERVER_NO_PORT": { "auth": "$CI_DEPENDENCY_PROXY_AUTH_TOKEN" }
    }
}
EOF
