#!/bin/sh

set +e

git config --global user.name "$GITLAB_USER_NAME"
git config --global user.email "$GITLAB_USER_EMAIL"

eval $(ssh-agent -s)

# Ensure the SSH key ends with a newline
sed -i -e '$a\' "$SSH_PRIVATE_KEY"

tr -d '\r' < "$SSH_PRIVATE_KEY" | ssh-add -

mkdir -p ~/.ssh
chmod 700 ~/.ssh

ssh-keyscan -H "$CI_SERVER_HOST" >> ~/.ssh/known_hosts

git config remote.push-to-here.url >&- || git remote add push-to-here "git@${CI_SERVER_HOST}:${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}.git"
