ARG IMAGE_PREFIX=
FROM ${IMAGE_PREFIX}golang:1.23-alpine

RUN apk --no-cache add \
    curl \
    git \
    gcompat \
    openssh-client \
    unzip

# renovate: datasource=github-releases depName=buf packageName=bufbuild/buf
ARG BUF_VERSION=1.50.0
# renovate: datasource=github-releases depName=protoc_gen_go packageName=protocolbuffers/protobuf-go
ARG PROTOC_GEN_GO_VERSION=1.36.3
# renovate: datasource=github-releases depName=protoc_gen_connect_go packageName=connectrpc/connect-go
ARG PROTOC_GEN_CONNECT_GO_VERSION=1.18.1

RUN go install github.com/bufbuild/buf/cmd/buf@v${BUF_VERSION} && \
    go install google.golang.org/protobuf/cmd/protoc-gen-go@v${PROTOC_GEN_GO_VERSION} && \
    go install connectrpc.com/connect/cmd/protoc-gen-connect-go@v${PROTOC_GEN_CONNECT_GO_VERSION}
