ARG IMAGE_PREFIX=
FROM ${IMAGE_PREFIX}amazoncorretto:21.0.5 as builder

RUN yum -y install \
    curl \
    unzip && \
    yum -y clean all && \
    rm -rf /var/cache

# renovate: datasource=github-releases depName=buf packageName=bufbuild/buf
ARG BUF_VERSION=1.50.0
# renovate: datasource=github-releases depName=connect_kotlin packageName=connectrpc/connect-kotlin
ARG CONNECT_KOTLIN_VERSION=0.7.2
# renovate: datasource=github-releases depName=gradle packageName=gradle/gradle
ARG GRADLE_VERSION=8.12
# renovate: datasource=github-releases depName=protoc packageName=protocolbuffers/protobuf
ARG PROTOC_VERSION=29.3

RUN curl -L -o gradle-bin.zip \
    "https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip" && \
    unzip -d /opt gradle-bin.zip && \
    # We cannot pass the variable version into the final image, so we hard-code the name here
    mv /opt/gradle-${GRADLE_VERSION} /opt/gradle

RUN curl -L -o /protoc-gen-connect-kotlin.jar \
    "https://github.com/connectrpc/connect-kotlin/releases/download/v${CONNECT_KOTLIN_VERSION}/protoc-gen-connect-kotlin-${CONNECT_KOTLIN_VERSION}.jar"

# Get the Google common protobuf definitions
RUN curl -L -o protoc-linux-x86_64.zip \
    "https://github.com/protocolbuffers/protobuf/releases/download/v${PROTOC_VERSION}/protoc-${PROTOC_VERSION}-linux-x86_64.zip" && \
    unzip -d /usr/local protoc-linux-x86_64.zip

# Get buf executable
RUN curl -L -o /usr/local/bin/buf \
    "https://github.com/bufbuild/buf/releases/download/v${BUF_VERSION}/buf-$(uname -s)-$(uname -m)" && \
    chmod +x /usr/local/bin/buf

ARG IMAGE_PREFIX=
FROM ${IMAGE_PREFIX}amazoncorretto:21.0.5

COPY --from=builder /opt/gradle/bin /usr/local/bin/
COPY --from=builder /opt/gradle/lib /usr/local/lib/
COPY --from=builder /usr/local/bin/buf /usr/local/bin/
COPY --from=builder /usr/local/bin/protoc /usr/local/bin/
COPY --from=builder /usr/local/include/google/ /usr/local/include/google/
COPY --from=builder /protoc-gen-connect-kotlin.jar /usr/local/bin/

RUN echo "#!/bin/sh" > /usr/local/bin/protoc-gen-connect-kotlin && \
    echo "exec java -jar /usr/local/bin/protoc-gen-connect-kotlin.jar" >> /usr/local/bin/protoc-gen-connect-kotlin && \
    chmod +x /usr/local/bin/protoc-gen-connect-kotlin
