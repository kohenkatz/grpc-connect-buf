ARG IMAGE_PREFIX=
FROM ${IMAGE_PREFIX}node:22.13-alpine3.21

RUN apk --no-cache add \
    nodejs \
    npm \
    wget
