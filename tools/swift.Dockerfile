ARG IMAGE_PREFIX=
FROM ${IMAGE_PREFIX}swift:6.0.3-noble as builder

RUN apt-get update && apt-get install -y \
    make \
    unzip \
    wget \
    && rm -rf /var/lib/apt/lists/*

# Get the Google common protobuf definitions
# renovate: datasource=github-releases depName=protoc packageName=protocolbuffers/protobuf
ARG PROTOC_VERSION=29.3
RUN wget https://github.com/protocolbuffers/protobuf/releases/download/v${PROTOC_VERSION}/protoc-${PROTOC_VERSION}-linux-x86_64.zip && \
    unzip -d /usr/local protoc-${PROTOC_VERSION}-linux-x86_64.zip

# Get and build protoc-gen-swift
# renovate: datasource=github-releases depName=swift-protobuf packageName=apple/swift-protobuf
ARG SWIFT_PROTOBUF_VERSION=1.28.2
RUN wget https://github.com/apple/swift-protobuf/archive/refs/tags/${SWIFT_PROTOBUF_VERSION}.zip && \
    unzip ${SWIFT_PROTOBUF_VERSION}.zip && \
    rm ${SWIFT_PROTOBUF_VERSION}.zip && \
    mv swift-protobuf-${SWIFT_PROTOBUF_VERSION} swift-protobuf && \
    cd swift-protobuf && \
    swift build -c release --static-swift-stdlib -Xlinker -s

# Get and build protoc-gen-connect-swift
# renovate: datasource=github-releases depName=connect_swift packageName=connectrpc/connect-swift
ARG CONNECT_SWIFT_VERSION=1.0.0
RUN wget https://github.com/connectrpc/connect-swift/archive/refs/tags/${CONNECT_SWIFT_VERSION}.zip && \
    unzip ${CONNECT_SWIFT_VERSION}.zip && \
    rm ${CONNECT_SWIFT_VERSION}.zip && \
    mv connect-swift-${CONNECT_SWIFT_VERSION} connect-swift && \
    cd connect-swift && \
    swift build -c release --static-swift-stdlib -Xlinker -s --product protoc-gen-connect-swift && \
    swift build -c release --static-swift-stdlib -Xlinker -s --product protoc-gen-connect-swift-mocks

# Get buf executable
# renovate: datasource=github-releases depName=buf packageName=bufbuild/buf
ARG BUF_VERSION=1.50.0
RUN wget "https://github.com/bufbuild/buf/releases/download/v${BUF_VERSION}/buf-$(uname -s)-$(uname -m)" -O /usr/local/bin/buf && \
    chmod +x /usr/local/bin/buf

ARG IMAGE_PREFIX
FROM ${IMAGE_PREFIX}ubuntu:noble

RUN apt-get update && apt-get install -y \
    git \
    openssh-client \
    && rm -rf /var/lib/apt/lists/*

COPY --from=builder /swift-protobuf/.build/release/protoc-gen-swift /usr/local/bin/
COPY --from=builder /connect-swift/.build/release/protoc-gen-connect-swift /usr/local/bin/
COPY --from=builder /connect-swift/.build/release/protoc-gen-connect-swift-mocks /usr/local/bin/
COPY --from=builder /usr/local/bin/buf /usr/local/bin/
COPY --from=builder /usr/local/include/google/ /usr/local/include/google/
