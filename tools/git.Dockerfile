ARG IMAGE_PREFIX=
FROM ${IMAGE_PREFIX}alpine:3.21

RUN apk --no-cache add \
    git \
    openssh-client
