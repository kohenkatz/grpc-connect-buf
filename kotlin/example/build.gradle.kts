import at.schrottner.gradle.GitlabRepositoriesExtension
import at.schrottner.gradle.auths.GitLabTokenType

plugins {
    id(libs.plugins.java.library.get().pluginId)
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.maven.publish)
    alias(libs.plugins.gitlab.repositories)
}

dependencies {
    api(libs.kotlin.stdlib)
    api(libs.kotlinx.coroutines.android)

    api(libs.protobuf.kotlin.lite)
    implementation(libs.connect.kotlin.okhttp)
    api(libs.connect.kotlin.javalite.ext)
}

sourceSets {
    main {
        java {
            srcDirs("generated/java")
        }
        kotlin {
            srcDirs("generated/connect-kotlin", "generated/kotlin")
        }
    }
}

kotlin {
    jvmToolchain(17)
}

val versionString: String = if (project.hasProperty("VERSION")) {
    project.property("VERSION") as String
} else if (System.getenv("CI_COMMIT_SHORT_SHA") != null) {
    System.getenv("CI_COMMIT_SHORT_SHA") + "-SNAPSHOT"
} else {
    "UNRELEASED"
}

val gitLabPrivateToken: String? by project

configure<GitlabRepositoriesExtension> {
    baseUrl = "gitlab.com"

    token(GitLabTokenType.PRIVATE) {
        key = "private"
        value = gitLabPrivateToken
    }
}

mavenPublishing {
    coordinates("com.example", "grpc-connect-buf", versionString)
}

publishing {
    repositories {
        maven {
            gitLab.project("44026207").execute(this)
        }
    }
}
