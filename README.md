# Building gRPC and Connect libraries using `buf` in GitLab CI

This repository contains the Protobuf definitions for a sample gRPC/Connect service.
It builds libraries for Go, JavaScript, Kotlin, and Swift.

- Go library releases are identified by semantic version tags beginning with `v`.
- JavaScript library releases are published to the GitLab NPM repository.
- Kotlin library releases are published to the GitLab Maven repository.
- Swift library releases are identified by semantic version tags **NOT** beginning with `v`.

The structure is loosely inspired by [this blog post](https://www.bugsnag.com/blog/libraries-for-grpc-services).

Although the [connect](https://connect.build/) libraries support communicating with a gRPC server/client,
we have chosen to keep generating the gRPC libraries too, in order to illustrate how this can be done.
This allows publishing backwards-compatible releases while migrating projects from gRPC to Connect.

***IMPORTANT:* DO NOT create your own tags in this repository.**
Instead, commit updates to the `VERSION` file, and the CI jobs will generate a new tag from there.
This is necessary because the build process needs to run before the tags can be created.

## Updating dependencies

This repository contains configuration for [Renovate Bot](https://docs.renovatebot.com/) to automatically update the dependencies.

There are also some "hints" in the `Dockerfile`s to help Renovate Bot find the correct packages.

## How to use this package

Note: all of the instructions below use `gitlab.com` as the domain because that is where this sample project is hosted.
In all cases, you can use your own self-hosted GitLab by replacing all instances of `gitlab.com` with your own server's name.

### From Golang

```bash
go get -u gitlab.com/kohenkatz/grpc-connect-buf/example
```

**NOTE:** If you use git over HTTPS, the `go get` command above will just work. However, if you
normally use git over SSH, you can run the following command to make `go get` use SSH too.
*This command must be run once on your system, but running it extra times will not make a difference.*

```bash
git config --global url."git@gitlab.com/".insteadOf https://gitlab.com/
```

Alternatively, you can create a `~/.netrc` file (or `$HOME/_netrc` on Windows) to use Git over HTTPS.  
See the instructions for that in the Swift instructions below.

You must also set your `GOPRIVATE` variable to tell Go to ignore the central repository:

```bash
go env -w GOPRIVATE=gitlab.com
```

### Kotlin (Android)

To use this repository, add the following block to your `app/build.gradle.kts`:

```kotlin
val gitLabPrivateToken: String by project

configure<GitlabRepositoriesExtension> {
    baseUrl = "gitlab.com"

    token(PrivateToken::class.java) {
        key = "private"
        value = gitLabPrivateToken
    }
}

repositories {
    // ...

    val gitLab = the<GitlabRepositoriesExtension>()

    maven {
        gitLab.project("44026207").execute(this)
        content {
            includeGroup("com.example")
        }
    }
}
```

**You must also authenticate to GitLab in your Gradle properties:**

(NOTE: If you are using multiple Maven repositories on this server, you only have to do this once.)

1. Go to https://gitlab.com/-/profile/personal_access_tokens?name=Grade+on+YOUR_COMPUTER_NAME&scopes=api
1. Create a new token (change `YOUR_COMPUTER_NAME` so you can keep track of where you are using this token).
   (This link has the correct scope pre-filled, do not change it.)
1. Edit the file `~/.gradle/gradle.properties` (create it if it does not exist) to add this line:

   ```groovy
   gitLabPrivateToken=THE_TOKEN_YOU_JUST_CREATED
   ```

### Swift (iOS)

To use this repository, add it to your Swift Package Manager dependencies in Xcode.

**You must also authenticate to GitLab in your `~/.netrc` file:**

(NOTE: If you are using multiple Swift repositories on this server, you only have to do this once.)

1. Go to https://gitlab.com/-/profile/personal_access_tokens?name=SPM+on+YOUR_COMPUTER_NAME&scopes=api
1. Create a new token (change `YOUR_COMPUTER_NAME` so you can keep track of where you are using this token).
   (This link has the correct scope pre-filled, do not change it.)
1. Edit the file `~/.netrc` (create it if it does not exist) to add these line:

   ```netrc
   machine gitlab.com
   login gitlab-ci-token
   password THE_TOKEN_YOU_JUST_CREATED
   ```

1. If Xcode prompts for login credentials for GitLab, use the same Personal Access Token as the password.
   (Note that Xcode may prompt for these credentials again sometimes. You do not need to generate new ones every time, just open your `.netrc` and copy from there.)

## How to build this package

See the code in the `.gitlab-ci.yml` file for how this package is built.

The `.proto` files in this repository get built into packages for the following languages:

- **Go** - Files are updated in `build:go` job and tagged in `bump-version` job.
- **JavaScript** (with TypeScript definitions) - NPM library is built and published in `package:browser` job.
- **Kotlin for Android** - Maven library is built and published in `package:kotlin` job.
- **Swift for iOS** - Files are updated in `build:swift` job and tagged in `bump-version` job.

### Docker images

This repository also contains CI scripts to build four Docker images:

- `browser-builder` - Builds JS files
- `go-builder` - Builds Golang files
- `git-builder` - Has `git` and `ssh` executables for making Git tags.
- `kotlin-builder` - Builds Kotlin files
- `swift-builder` - Builds Swift files
